function GM:PlayerInitialSpawn( ply ) --"When the player first joins the server and spawns" function
 
    ply:ConCommand( "team_menu" ) --Run the console command when the player first spawns
 
end --End the "when player first joins server and spawn" function

function GM:PlayerLoadout( ply ) --"The weapons/items that the player spawns with" function
 
	ply:StripWeapons() -- This command strips all weapons from the player.
 
	if ply:Team() == 1 then --If the player is on team "Guest"...
		ply:Give( "weapon_physcannon" ) -- ...then give them the Gravity Gun.
 
	elseif ply:Team() == 2 then -- Otherwise, if the player is on team "Another Guest"...
		ply:Give( "weapon_physgun" ) -- ...then give them the Phys Gun.
 
	end -- This ends the if/elseif.
 
end -- This ends the function.
 
function team_1( ply )
 
    ply:SetTeam( 1 )
 
end
 
function team_2( ply )
 
    ply:SetTeam( 2 )
end
 
concommand.Add( "team_1", team_1 )
concommand.Add( "team_2", team_2 )
 
 